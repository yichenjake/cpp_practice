
#include <algorithm>
#include <iostream>
#include <cassert>

using namespace std;

template <typename II1, typename II2>
bool my_equal(II1 b1, II1 e1, II2 b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;
    }
    return true;
}

/**
 * Categories of Iterators
 * 1. input iterator
 *      ==, !=, * (read only), ++
 * 
 * 2. output iterator
 *      * (write only), ++
 * 
 * 3. forward iterator
 *      ==, !=, * (read/write), ++
 * 
 * 4. bidirectional iterator
 *      ==, !=, * (read/write), ++, --
 * 
 * 5. random access iterator (this is the category of a pointer)
 *      ==, !=, * (read/write), ++, --, [], +, -, <, >, <=, >=
 * 
 * 
 * When you design an algorithm,
 *  rely on the weakest category of iterator
 * 
 * When you design a container,
 *  provide the strongest category of iterator
 *      ex. linkedlist (biderectional iterator)
 */

// class Yichen {
//     class iterator {
//         bool operator == (...)
//     }
// }

/*
Limitations of stack allocated arrays
*/

// same address
void copy1() {
    int a[] = {2, 3, 4};
    // int b[] = a;            // compile error: fail to determine size of b
    int *b = a;
    assert(a == b);
    assert(sizeof(a) != sizeof(b));
    assert(sizeof(a) == 12);
    assert(sizeof(b) == 8);
    ++b[1];
    assert(equal(a, a + 3, begin({2, 4, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));
}

template <typename T>
void print_array(T arr[], size_t size) {
    size_t i;
    for (i = 0; i < size; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

template <typename II, typename OI>
OI my_copy(II b1, II e1, OI b2) {
    while (b1 != e1) {
        *b2 = *b1;
        ++b1;
        ++b2;
    }
    return b2;
}

void copy2() {
    int a[] = {2, 3, 4};
    const size_t s = sizeof(a) / sizeof(a[0]);
    int b[s];
    copy(a, a + s, b);
    // my_copy(a, a + s, b);
    print_array(b, s);
}


int main() {
    copy1();
    copy2();
}


