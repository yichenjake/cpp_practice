
#include <algorithm>    // equal
#include <array>
#include <iostream>
#include <cassert>
#include <list>
#include <vector>

using namespace std;

template <typename T> 
void print_array(const T arr[], const size_t size) {
    size_t i;
    for (i = 0; i < size; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

template <typename T>
void builtin_array() {
    int a[] = {2, 3, 4};
    cout << "int a[] is equivalent to int* const" << endl;  // ++a (not allowed)
    assert(*a == a[0]);
    assert(a == &a[0]);
    assert(sizeof(a) != sizeof(&a[0]));
    assert(sizeof(a) == 12);
    assert(sizeof(&a[0]) == 8);

    ++a[1];
    ++*(a + 1);
    assert(a[1] == 5);
    assert(*(a + 1) == 5);
    
    // cout << "indexing out of bounds (undefined value): " << a[3] << endl;  // give a warning, but compiles, the value is undefined


    const size_t s = 10;
    const int b[s] = {2, 3, 4};     // O(n), 7 zeros
    const T   x[s] = {2, 3, 4};     // O(n), T(int) 3 times, T() 7 times
    cout << "sizeof int b[10]: " << sizeof(b) << endl;
    print_array(b, sizeof(b) / sizeof(b[0]));
    cout << "sizeof T x[10]: " << sizeof(x) << endl;
    print_array(x, sizeof(x) / sizeof(x[0]));

    // ++a;                         compile error
    // ++b[1];                      read-only location
}

template <typename T>
void builtin_array2() {

    /* asymmetry between a built-in type array and a generic type array */
    const size_t s = 10;
    // const int a[s];                 // error: default initialization of an object of const type 'const int [10]'
    int a[s];                          // O(1), undefined values
    cout << "undefined array: ";
    print_array(a, s);
    T x[s];                            // O(n), T() s times
    cout << "T x[s] is defined by default: ";
    print_array(x, s);                 

    const int b[s] = {};
    const T y[s] = {};
    print_array(b, s);
    print_array(x, s);

}


void lib_list() {
    int a[] = {2, 3, 4};
    int b[] = {2, 3, 4};
    assert(a != b);                     // O(1)
    // equal from algorithm 
    assert(equal(a, a + 3, b));         // O(n)

    // linkedlist
    list<int> c = {2, 3, 4};
    list<float> d = {2, 3, 4};
    list<int>::iterator b1 = begin(c);
    list<int>::iterator e1 = end(c);
    list<float>::iterator b2 = begin(d);
    assert(equal(b1, e1, b2));

    cout << *b1 << endl;
    ++b1;
    cout << *b1 << endl;
}

void lib_vector() {
    vector<int> a = {2, 3, 4};              // ArrayList
    vector<int>::iterator b = begin(a);
    cout << *b << endl;
    ++b;
    cout << *b << endl;
}



template <typename T, int N>
class MyArray {
        T a[N];
    public:
        T& set(int i, const T& v) {
            a[i] = v;
            return a[i];
        }

        const T& get(int i) {
            return a[i];
        }

        void print() {
            size_t i;
            const size_t n = sizeof(a) / sizeof(a[0]);
            for (i = 0; i < n; ++i) {
                cout << a[i] << " ";
            }
            cout << endl;
        }
};

int main() {
    // builtin_array<int>();
    // builtin_array2<int>();
    lib_list();

    // template practice
    // MyArray<long, 10> a;
    // cout << a.get(0) << endl;
    // a.set(0, 10);
    // cout << a.get(0) << endl;
    // a.print();


    return 0;
}


