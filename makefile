.DEFAULT_GOAL := all

ASTYLE        := astyle
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-10
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    LIB      := /usr/local/lib
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    LIB      := /usr/lib
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    LIB      := /usr/local/lib
endif

FILES := arrays \
		 equal  \
		 allocator
		 #  more_arrays \
		 

all: $(FILES)

clean:
	-rm arrays 
	-rm equal
	-rm more_arrays 
	-rm allocator
	-rm -rf *.gcda
	-rm -rf *.gcno