

template <typename T>
void heap_array() {
    size_t s = 10;
    int* a = new int[s];        // O(1), undefined values
    T* x = new T[s];            // O(n), T()
    const int v = 5;
    fill(x, x + s, v);          // O(n), copy assignment operator
    delete [] x;                // O(n), ~T()
}


// void real_allocator() {
//     allocator<T> x;             // default constructor
//     T v(...);
//     T* a = x.allocate(s);       // O(1), no method of T runs, under the hood == new
//     T* b = a;
//     T* e = a + s;
//     while (b != e) {            // O(n), T(T), copy constructor s times
//         x.construct(b, v);
//         ++b;
//     }

//     b = a;
//     while (b != e) {            // O(n), ~T()
//         x.destroy(b);
//         ++b;
//     }
//     x.deallocate(a, s);         // O(1), delete
// }

int main() {
    heap_array<int>();
}

