
#include <iostream> 

using namespace std;

int main() {
   int i = 1;
   int* i_pointer = &i;
   cout << *i_pointer << endl;
   
   ++*i_pointer;
   cout << "incremented i: " << *i_pointer << endl;
   
   const int j = 10;
   const int* j_pointer = &j;
   cout << "j: " << *j_pointer << endl;;
   
   // ++*j_pointer;
   cout << "a const type can not be changed: " << *j_pointer << endl;


   return 0;
}



