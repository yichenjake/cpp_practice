

#include <algorithm>
#include <cassert>
#include <iostream>
#include <array>

using namespace std;

// misleading
void f (int p[]) {
    assert(sizeof(p) == 8);         // warning: ‘sizeof’ on array function parameter ‘p’ will return size of ‘int*’
    ++p;
    ++p[0];
    ++*p;
}

void g (int* p) {
    assert(sizeof(p) == 8);  
    ++p;
    ++p[0];
    ++*p;
}


/*
no array copy
no array assignment
no array passing by value
*/
void array1() {
    int a[] = {2, 3, 4};
    // f(a);
    g(a);
    assert(equal(a, a + 3, begin({2, 5, 4})));
    int b[] = {3, 4, 5};
    // b = a;                       // invalid, bc [] is int* const
}

void h1(array<int, 3>& y) {
    array<int, 3>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;
}

/**
 * 1. same initialization as a builtin array
 * 
 * Things stl_array can do:
 * 1. size()
 * 2. indexing
 * 3. copy
 * 4. comparison
 * 5. pass by value
 */
void stl_array() {
    // compile time: size
    array<int, 3> x = {2, 3, 4};
    assert(x.size() == 3);
    assert(x[1] == 3);
    array<int, 3> y(x);
    assert(y == x);
    assert(&y[0] != &x[0]);

    y = {4, 5, 6};
    assert(y != x);
    y = x;
    assert(y == x);

    array<int, 3> z = {2, 3, 4};
    h1(z);
    assert(equal(begin(z), end(z), begin({2, 5, 4})));
}


template <typename FI, typename T>
void my_fill(FI b, FI e, const T& v) {              // could modify v / cannot tolerate rvalue => const
    while (b != e) {
        *b = v;
        ++b;
    }
}

template <typename II, typename T>
size_t my_count(II b, II e, T v) {
    size_t c = 0;
    while (b != e) {
        if (*b == v)
            ++c; 
        ++b;
    }
    return c;
}

void heap_array() {
    const ptrdiff_t s = 10;
    const int v = 2;
    int* const a = new int[s];      // O(1)
    // T* const x = new int[s];     // O(n)
    assert(sizeof(a) == 8);
    my_fill(a, a + s, v);
    assert(my_count(a, a + s, v) == s);
    delete [] a;                    // O(1)
    // delete [] x;                    // O(n) ~T()
}

int main() {
    array1();
    stl_array();
    heap_array();
}

